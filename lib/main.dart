import 'package:flutter/material.dart';
import 'package:sample_todo_app/home.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.blue[800],
        accentColor: Colors.blue[500],
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        tabBarTheme: TabBarTheme(
            indicatorSize: TabBarIndicatorSize.tab,
            labelColor: Color(0xFFE3F2FD),
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 3, color: Color(0xFF42A5F5)))),
        buttonTheme: ButtonThemeData(
            buttonColor: Color(0xFF536DFE), textTheme: ButtonTextTheme.primary),
      ),
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
