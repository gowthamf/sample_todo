import 'package:flutter/material.dart';

class AddTodo extends StatefulWidget {
  final Function addTodo;

  AddTodo({this.addTodo});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddTodo();
  }
}

class _AddTodo extends State<AddTodo> {
  final _todoTextController = TextEditingController();
  final _textKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Form(
            key: _textKey,
            child: TextFormField(
              controller: _todoTextController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter an Item';
                }
                return null;
              },
              decoration: InputDecoration(
                  hintText: 'Enter something',
                  contentPadding:
                      EdgeInsets.only(top: 0, bottom: 10, left: 10, right: 10)),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 10),
          alignment: Alignment.topRight,
          child: RaisedButton(
            onPressed: () {
              if (_textKey.currentState.validate()) {
                widget.addTodo(_todoTextController.text);
              }
            },
            child: Text('ADD'),
          ),
        ),
      ],
    );
  }
}
