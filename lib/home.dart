import 'package:flutter/material.dart';
import 'package:sample_todo_app/add_todo.dart';
import 'package:sample_todo_app/todo_list.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home> {
  List<String> todo = List<String>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Center(child: Text('TODO')),
          ),
          body: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                AddTodo(
                  addTodo: _addTodo,
                ),
                SizedBox(
                  height: 50,
                ),
                ToDoList(
                  todo: todo,
                )
              ],
            ),
          )),
    );
  }

  void _addTodo(String item) {
    setState(() {
      todo.add(item);
    });
  }
}
