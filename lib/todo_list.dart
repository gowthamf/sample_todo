import 'package:flutter/material.dart';

class ToDoList extends StatefulWidget {
  final List<String> todo;

  ToDoList({this.todo});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ToDoList();
  }
}

class _ToDoList extends State<ToDoList> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Container(
              height: 50,
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(widget.todo[index]),
                      Container(
                          padding: EdgeInsets.only(right: 50),
                          child: IconButton(
                              splashColor: Colors.black12,
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                setState(() {
                                  widget.todo.removeAt(index);
                                });
                              }))
                    ],
                  ),
                ],
              ));
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: widget.todo.length);
  }
}
